SET search_path TO data;
create table indicator
(
    id bigint NOT NULL ,
    name varchar NOT NULL ,
    unit varchar(5) NOT NULL ,
    CONSTRAINT pk_indicator PRIMARY KEY (id),
    CONSTRAINT uk_indicator UNIQUE (name)
);
comment on table indicator is 'Показатели';
comment on column indicator.name is 'Наименование показателя';
comment on column indicator.unit is 'Единицы измерения';
create table country
(
    id bigint NOT NULL ,
    name varchar NOT NULL ,
    continent varchar NOT NULL ,
    CONSTRAINT pk_country PRIMARY KEY (id),
    CONSTRAINT uk_country_name UNIQUE (name),
    CONSTRAINT uk_country_name_continent UNIQUE (name, continent)
);
comment on table country is 'Страны и континенты';
comment on column country.name is 'Название страны';
comment on column  country.continent is 'Название континента';
create table history_indicators
(
    id bigint NOT NULL ,
    id_country bigint NOT NULL ,
    id_indicator bigint NOT NULL ,
    value NUMERIC NOT NULL ,
    date_act date NOT NULL ,
    CONSTRAINT pk_history_indicators PRIMARY KEY (id),
    CONSTRAINT uk_history_indicators UNIQUE (id_country, id_indicator, date_act),
    FOREIGN KEY (id_country) REFERENCES country(id),
    FOREIGN KEY (id_indicator) REFERENCES indicator(id)
);
comment on table history_indicators is 'Показатели за разные периоды времени';
comment on column history_indicators.id_country is 'ИД страны';
comment on column history_indicators.id_indicator is 'ИД показателя';
comment on column history_indicators.value is 'Значение показателя';
comment on column history_indicators.date_act is 'Дата актуализации';